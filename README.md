# **WARNING: This code and Module are optimized for Google Chrome WHEN USING WINDOWS and SAFARI when using Mac. If you use a different browser, you're going to have a problem**

The Emperor of the the Blox Galaxy has passed away. As is tradition, a new leader must be crowned, and thus the Star Blox Tourament Begins anew!
You and your brother are the last two remaining competitors in the Tournament. You must now fight to the death amongst the stars to see who will be crowned and who will perish.

Names: Drew Brost and Max Lowenthal  
Student IDs: 431940 and 435479  
[Here's a link to our game!](http://ec2-54-186-115-66.us-west-2.compute.amazonaws.com/~stilkin/creative/3d/instructions.html)  

_Quick Summary of What we Made_: 
	Inspired by a game from our childhood, we decided to build a sort of head to head block knocking game. The basic premise is that each of the two players controls
	a sphere, which is able to slam the ground and send all of the blocks in a straight line in front of them falling. The goal is to out manuever and trap your opponent
	such that they are trapped on a block when it falls and they eventually lose a life. The controls are outlined in more detail on the link above, you may want to play it a few times
	to get a feel for the game.
	
Rubric: 
***Admin***
20 Creative
5 Rubric Submitted on time

***Pre-Game***
5 Create user, and enter game
5 select character color

***Game-Functionality***
5 Characters move
5 Characters face direction last moved
5 Characters can “slam”
5 Lose a life for standing on black
5 lose game if out of life
2 timer 
5 timer shrinks board
5 two player functionality

***Using The Technologies***
10 Using EaselJS 
10 Using TweenJs
(or 20 for using three.js)

***Post-Game***
3 game over screen

***Best Practices***
2 well-formatted and commented code
3 passes validator
5 visual appeal

***Creative Portion:***
Unlike the traditional creative portion, we've added various features to increase the overall functionality of our game, there are quite a few, but we'll outline the most important
	ones below.

Part I: Sound
	What's a good video game without some tunes? We've implemented a Javascript system to play music on the background of the character select, game over, and gameplay screens
	throughout the game. We picked these songs based on the vibes we intended to give the player on each of the screens. In addition to game music, we also personally recorded voice lines
	for each of the color options the players can choose. If you select any of the Red based color options, you'll hear Max's voice name the color outloud. If you select any of the blue 
	based color options, you'll hear Drew say something (I reccomend Royal Blue). However, due to the way we stored the sound files (on Google Drive, not locally on a directory) there is a bit 
	of a limitation to this feature. If Google Drive detects the file being opened (IE the page loading) too many times in one day, it'll stop loading the content as a security measure. So if you open the console
	and the sound isnt found, that's your issue. Just wait a bit and itll come back.

Part II: TweenJs	
	As our rubic above outlines, you can recieve either 20 points for the use of EaselJs and TweenJs, or 20 points for the use of the 3D Javascript rendering language ThreeJS. 
	While we chose to implement ThreeJS in order to give our game a more 3D realisitc feel to it, moving without the animations of TweenJS just didnt feel natural. As a result, 
	all movements in the game, from the characters themselves to the blocks changing colors and falling, are written completely using TweenJs animtation. The game now flows much smoother
	compared to the alternative of just redrawing ThreeJS objects each time you move or take an action.

Part III: Directional Shift
	While we did outline in our rubric that characters face the direction they last moved, we quickly realized that this was a limiting idea. As a result we created an option for both players
	to push buttons that allow them to stay in the same space and rotate the direction they are facing, which is now denoted by an Arrow sticking out of each sphere. For Player 1 pushing Q & E
	allow the player to shift either left or right. For Player 2 by pushing Control or Shift, the same action occurs.
	
Part IV: Texturing and Background
	While we orginally intended to just make a beta version of this game for the project, free of any sytlistic elements, we decided to take it a step further. By making the background of the ThreeJS
	canvas clear (which was not an easy task) we were able to place a space background on our game, giving it a more video game style feel. We also chose to texture map the entire game board with a cloud-like
	image in order to make the board pop when compared to its new background

Part V: Kill Counter
	As in many video games, the Kill to Death ratio is a good indicator of how strong a player's abilities are. While each player starts with 5 lives, we've implemented a kill counter
	which is displayed for the winner on the Game Over screen, so that players can see how good they actually are. The counter increases by 1 for every kill a player gets, and decreases by 1 for every death.
	Unfortunately for some players, the counter DOES NOT increase if a player falls to their doom on a slammed block of their own color, which happens pretty frequently in such a fast paced game.


