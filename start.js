
this.charColorP1 = "FF0000"; //red
this.charColorP2 = "0000CD"; //blue

//Global variables to be accessed by all functions
var stage;
var p1Circle;
var p2Circle;
var readyP1 = false;
var readyP2 = false;
var nameP1;
var nameP2;
var canvas;
var context;
var stage;

//function runs on page load, it creates the canvas for the image to be loaded and sets the button colors
function startScreen(){
stage = new createjs.Stage("theCanvas1");
canvas =  document.getElementById("theCanvas1");
context  = canvas.getContext('2d');

stage.canvas.width = window.innerWidth;
stage.canvas.height = window.innerHeight;



imageLoader('FF0000.png','0000CD.png',0,2,canvas,context,stage);
document.getElementById("readyUp1").setAttribute("style", "background-color:#" + this.charColorP1 +"; border-color:#" +this.charColorP2 + "; border-width: medium medium medium medium;" );
document.getElementById("readyUp2").setAttribute("style", "background-color:#" + this.charColorP2 +"; border-color:#" + this.charColorP1 +"; border-width: medium medium medium medium;");


}

//x and y are the names of the colors in hex
// j is the value that i starts at, allows to change the second sphere 
// z is the number of times it runs
//canvas, context, and stage are all needed elements to draw the images
//
// the function itself takes in all of these things, and draws the images
function imageLoader(x,y,j,z,canvas,context,stage){
	var width = 200;
	var height = 200;
	
	var imgArray = [x,y];

	for(var i = j; i<z; i++){
		var imageObj = new Image();
		imageObj.src = imgArray[i];
		if (i===0){
			imageObj.setAtX = (stage.canvas.width*.20);
		}
		else{
			imageObj.setAtX = (stage.canvas.width*.75);
		}
		imageObj.setAtY = (stage.canvas.height*.25);
		imageObj.onload = function(){
			context.drawImage(this, this.setAtX, this.setAtY, width, height);
		}
	}
}

//plays the different sounds for the color loads
function sound1(name){
	var sound = document.createElement("AUDIO");
	sound.setAttribute("src",name);
	sound.load();
	sound.play();
}

//This method changes the color of the sphere for player1 on the select screen, it also changes the button color and border for both players and plays color sounds
function changeP1C(){
	if(document.getElementById("p1C").value == 800000){
		sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5Ql9GN1A2bDZ5U2c");
	}
	else if(document.getElementById("p1C").value == "FF00FF"){
	sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5eEtMXzNOQ05JeXc");
	}
	else if(document.getElementById("p1C").value == "FF4500"){
	sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5ZmRLYW1iUjBiNHc");
	}
	else if(document.getElementById("p1C").value == "DC143C"){
	sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5NFZFYVVHZWJ6S1E");
	}
	else if(document.getElementById("p1C").value == "DA70D6"){
	sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5Tl9GcENNN3R6ZHc");
	}
	else if(document.getElementById("p1C").value == "E9967A"){
	sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5RnlITWNCb05xRnc");
	}
	else if(document.getElementById("p1C").value == "FF0000"){
	sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5SURpdVVtUWY4dnc");
	}
	
	

	else{}
	this.charColorP1 = document.getElementById("p1C").value;
	canvas =  document.getElementById("theCanvas1");
	context  = canvas.getContext('2d');
	stage = new createjs.Stage("theCanvas1");

	imageLoader(this.charColorP1+".png",this.charColorP2+".png",0,1,canvas,context,stage);
	
	document.getElementById("readyUp1").setAttribute("style", "background-color:#" + this.charColorP1 +"; border-color:#" + this.charColorP2 + "; border-width: medium medium medium medium;"  );


}

//this method does the same as above but for player 2 when called
function changeP2C(){
	if(document.getElementById("p2C").value == "0000CD"){
		sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5TWZfU3BhT2QtWUk");
	}
	else if(document.getElementById("p2C").value == "4B0082"){
	sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5eDFTdTFXNkxETFU");
	}
	else if(document.getElementById("p2C").value == "00FF7F"){
	sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5azRaazJQejhOalU");
	}
	else if(document.getElementById("p2C").value == "00FFFF"){
	sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5R043X1Q5S1JMYlE");
	}
	else if(document.getElementById("p2C").value == "4169E1"){
	sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5ZkJNRS1kZkZWQkE");
	}
	else if(document.getElementById("p2C").value == "006400"){
	sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5MmRtbl9MNDVkdms");
	}
	else if(document.getElementById("p2C").value == "808000"){
	sound1("https://drive.google.com/uc?id=0ByBm1rrfnFH5dlF4bC1GRGZZdk0");
	}
	
	
	
	
	
	
	this.charColorP2 = document.getElementById("p2C").value;
	canvas =  document.getElementById("theCanvas1");
	context  = canvas.getContext('2d');
	stage = new createjs.Stage("theCanvas1");

	imageLoader(this.charColorP1+".png",this.charColorP2+".png",1,2,canvas,context,stage); //so this is a bit complex, because we start 1 in the for loop, not 0, we want to run the function with the color want as the second image, not the first
 
	document.getElementById("readyUp2").setAttribute("style", "background-color:#" + this.charColorP2 +"; border-color:#" + this.charColorP1 + "; border-width: medium medium medium medium;" );
 
}

//changes the border of button for P1
function changeP1CBord(){
document.getElementById("readyUp1").setAttribute("style", "background-color:#" + this.charColorP1 +"; border-color:#" + this.charColorP2 +  "; border-width: medium medium medium medium;" );
}


//changes the border of button for P2
function changeP2CBord(){
document.getElementById("readyUp2").setAttribute("style", "background-color:#" + this.charColorP2 +"; border-color:#" + this.charColorP1 +  "; border-width: medium medium medium medium;"  );
}

//makes P1 "ready" to start the game
function readyP111(){
	
	nameP1 = document.getElementById("userN1").value;
	if (nameP1 === ""){
		window.alert("You Need to Enter a Username!");
		}
	else{
		readyP1 = true;
		var rBox = document.getElementById("name1");
		rBox.innerHTML = "";
		rBox.innerHTML = "<div class='whiteWords'><h4>P1 Ready!</h4></div>";
	
		if (readyP2) {
			gameStart();
		}
	}

 
}

//makes P2 "ready" to start the game
function readyP222(){
	nameP2 = document.getElementById("userN2").value;
	
	
	if (nameP2 === "" && readyP1 == true){
		var rBox = document.getElementById("name1");
		rBox.innerHTML = "<div class='whiteWords'><h4>P1 Ready!</h4></div>";
		window.alert("You Need to Enter a Username!");
	}
	else if(nameP2 === ""){
		window.alert("You Need to Enter a Username!");
	}
	else{
		readyP2 = true;
		var rBox = document.getElementById("name2");
		rBox.innerHTML = "";
		rBox.innerHTML = "<div class='whiteWords'><h4>P2 Ready!</h4></div>";
		if (readyP1) {
			gameStart();
		}
	}
}

//sends info to the game to draw players names and such
function gameStart() {
	sessionStorage.getItem('userN1');
	sessionStorage.setItem('userN1', nameP1);
	
	sessionStorage.getItem('userN2');
	sessionStorage.setItem('userN2', nameP2);
	
	sessionStorage.getItem('userC1');
	sessionStorage.setItem('userC1', this.charColorP1);
	
	sessionStorage.getItem('userC2');
	sessionStorage.setItem('userC2', this.charColorP2);
	
	window.location.href = "gameRoom.html";
}
