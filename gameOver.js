var user = "Drew"; //name of the user who won
var stage;
var canvas;
var context; 
var timer = "1:00:00"; //time it took to win
var kills = 6; //kills they got when they won
this.charColor1 = "0000CD";
this.charColor2 = "FF0000";

function init(){ //this loads up the sphere of the winner, and the stats
	var user = sessionStorage.getItem('winnerName');
	
	if (user == null || user == -1) {
		window.location.href = "gameLobby.html"; //goes back to game lobby if someone tries to load just the gameOver screen
	}
	
	var charColor1 = sessionStorage.getItem('winnerColor');
	var kills = sessionStorage.getItem('winnerKills');
	var timer = Math.floor(sessionStorage.getItem('times')/2)/10;
	
	stage = new createjs.Stage("theCanvas1");
	canvas =  document.getElementById("theCanvas1");
	context  = canvas.getContext('2d');
	
	stage.canvas.width = window.innerWidth;
	stage.canvas.height = window.innerHeight;
	
	imageLoader(charColor1 + '.png',canvas,context,stage);
	document.getElementById("winName").innerHTML = user + " Wins!";
	document.getElementById("time").innerHTML = "Time: " + timer;
	document.getElementById("kills").innerHTML = "Kills: " + kills;
	document.getElementById("buttonPA").setAttribute("style", "background-color:#" + this.charColorP1 +"; border-color:#" +this.charColorP2 );
}


function btn(){ //button functionality to redirect to home page if they wanna play again
    window.location.href = "gameLobby.html"; //change when uploaded to server
}


function imageLoader(x,canvas,context,stage){ //function for actually loading images
	var width = 300;
	var height = 300;
	
	var imgArray = [x];

		var imageObj = new Image();
		imageObj.src = imgArray[0];
		imageObj.setAtX = (stage.canvas.width*.370);
		imageObj.setAtY = (stage.canvas.height*.20);
		imageObj.onload = function(){
		context.drawImage(this, this.setAtX, this.setAtY, width, height);
		}
	}
