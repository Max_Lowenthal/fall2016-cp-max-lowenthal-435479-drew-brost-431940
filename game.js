// define reference values
window.my = {};
my.lives = 5; //player lives
my.dimensions = 9; //board dimensions
my.bufferH = 15; //board height buffer (for 2d version)
my.bufferW = 215; //board width buffer (for 2d version)

my.vibrateTime = 4; //when does the board shake
my.decayed = 8; //when does the board start to fall
my.respawnBlock = 24; //when does the board respawn
my.decayRate = 250; //time increment between decay steps

my.pMoveLag = 150; //time between being able to move
my.playerBuffy = 50; //vertical icon buffer, 2d version
my.playerBuffx = 50; //horiz icon buffer, 2d

my.edgeDecayFrequency = 20; //how often the edge decays
my.minimalDimensions = 5; //smallest the board gets 

//establish global variables, for features past present and future
var stage, w, h, buffer, squareH, squareW, squareD, playerR;
var gameB; //THE MAIN GAME
var cubeW = 50;
var pause = false;
var startTime = Date.now();
var container;
var camera, scene, HUD, renderer, cssRend, stats;
var backgroundScene, backgroundCamera; //THREE.js things
var yfloat = 50;
var HUDw, HUDh, HUDz;
var time=0;
var timeText;
var fallenRows = 0;
var availSpaces = my.dimensions*my.dimensions; //max number of players

class gameSpace {
	constructor(gameBo, i,j) {
		this.decay = 0; //ticker for decay
		this.occupied = false; //has a player
		this.defunctSpace = false; //edge has fallen
		this.player = null; 
		
		this.color = 0xffffff;
		this.color = new THREE.Color(this.color);

		this.gameBoard = gameBo;
		this.changed = true; //2d vestigial tail
		this.slammer = null;//person who slammed it
		this.edgeDecaying = false;
		 
		//make the avatar
		var geometry = new THREE.CubeGeometry( cubeW, cubeW/2, cubeW ); 
		var material = new THREE.MeshPhongMaterial( { map: THREE.ImageUtils.loadTexture('clouds.jpg') } );
		this.cube = new THREE.Mesh(geometry, material );
		this.cube.position.x = ((cubeW*i));
		this.cube.position.z = ((cubeW*j));
		this.xorigin = this.cube.position.x;
		this.zorigin = this.cube.position.z;
		this.cube.recieveShadow = true;
		scene.add( this.cube );
		
		//make the wireframe
		//these should be linked with an edit function, but I realized this too late
		var geo = new THREE.EdgesGeometry( geometry ); // or WireframeGeometry( geometry )
		var mat = new THREE.LineBasicMaterial( { color: "white", linewidth: 2 } );
		this.wireframe = new THREE.LineSegments( geo, mat );
		this.wireframe.position.x=((cubeW*i));
		this.wireframe.position.z = ((cubeW*j));
		scene.add( this.wireframe );
		//create a tween
		this.vibTween;

	}

	decayMaint() { //update the decay status
		if (this.defunctSpace && this.decay == 0) {
			//brute force solution to persistent problems with defunct spawning issues
			this.cube.position.y = 50000;
			this.wireframe.position.y = 50000;
		}
		if (this.decay == 0) {
			//do nothing
		} else if (this.decay >= my.respawnBlock) {
			//reset
			this.decay = 0;
		
		} else {
			//do all of the things
			this.decay++; //increment
			//darken color:
			if (this.decay < my.respawnBlock) {
				var decayColorRate = .9;
				this.color.r = this.color.r *decayColorRate;
				this.color.b = this.color.b*decayColorRate;
				this.color.g = this.color.g*decayColorRate;
				this.cube.material.color = this.color;
			}
			//start shaking:
			if (this.decay == my.vibrateTime) {
				this.vibrate();
			}
			//start to fall
			if (this.decay == my.decayed) {

				if (this.edgeDecaying) {
					this.edgeFall();
				} else {
					this.drop();
				}
			}
		}
	}

	drop() {
		var finalPos = -100000; //end point below the screen
		var position = {y: 0}; //start array
		var end = { y: finalPos}; //end array (for tween)

		//set up the tween
		var tween1 = new TWEEN.Tween(position).to(end, (my.respawnBlock - my.decayed)*my.decayRate);
		//prevent movement if on falling block
		if (this.occupied) this.player.stuck();
		//define what the tween does
		tween1.onUpdate(function(){
			//fall
			this.cube.position.y = position.y;
			this.wireframe.position.y = position.y;
			
			//player falls
			if (position.y != finalPos) {
				if (this.occupied ) {
					this.player.avatar.position.y = position.y + yfloat;
					this.player.arrow.position.copy(new THREE.Vector3(this.player.avatar.position.x,this.player.avatar.position.y,this.player.avatar.position.z));
				}
			} else {
				//restore things
				
				//if it has not been defuncted:
				if (!this.defunctSpace) {
					this.cube.position.y = 0;
					this.wireframe.position.y = 0;
				}
				//return to white
				this.color = new THREE.Color(0xffffff);
				this.cube.material.color = this.color;
				
				//deal with any pasengers
				if (this.occupied ) {
					//lives
					this.player.lives--;
					document.getElementById("p1").innerHTML = gameB.players[0].name + ":" + gameB.players[0].lives;
					document.getElementById("p2").innerHTML = gameB.players[1].name + ":" + gameB.players[1].lives;
					//kill credit
					if (this.player != this.slammer) {
						this.slammer.kills++;
					}
					//reset slammer
					this.slammer = null;
					//alive? respawn
					if (this.player.lives > 0) {
						this.gameBoard.respawnPlayer(this.player);
					} else { //dead? make 'em go away. 
							
						this.gameBoard.playersChanged = true;
						this.gameBoard.respawnPlayer(this.player);
						var index = this.gameBoard.players.indexOf(this.player);
						//if you've found them, remove them from the scene
						if (index > -1) {
							this.gameBoard.players.splice(index, 1);
							scene.remove(this.player.avatar);
							
							//credit a winner if there is one
							if (this.gameBoard.players.length == 1) {
									sessionStorage.getItem('winnerName');
									sessionStorage.setItem('winnerName', this.gameBoard.players[0].name);
									sessionStorage.getItem('winnerColor');
									sessionStorage.setItem('winnerColor', this.gameBoard.players[0].colorMem);
									sessionStorage.getItem('winnerKills');
									sessionStorage.setItem('winnerKills', this.gameBoard.players[0].kills);
									sessionStorage.getItem('times');
									sessionStorage.setItem('times', time);
									window.location.href = "gameOverScreen.html";
							}
						}
					}
					//housekeeping
					this.player = null;
					this.occupied = false;
				}
			}
		}.bind(this)); //important - uses this within function for tween
		
		tween1.easing(TWEEN.Easing.Quartic.In);//fall rate
		
		tween1.start(); //go!
		
	}

	vibrate() {
		//set parameters
		var numVibes = 10;
		var vibeTime = my.decayed - my.vibrateTime;
		var timeUnit = .5*vibeTime*2*my.decayRate/(numVibes)/(numVibes+1);//the amount of time per i
		var vibeDisp = 4; //assuming constant disp for now
		
		//offset to make vibrations visible
		this.cube.position.y += 1;
		if ((this.x + this.y)%2) this.cube.position.y += 1;  //Stagger in checkerboard pattern to avoid overlapping wireframes
		this.wireframe.position.y += 1;
		if ((this.x + this.y)%2) this.cube.position.y +=1;
		
		// i for scaling 
		var i=0;
		var timeSum = timeUnit*(numVibes - i +1);
		
		//distance to move the cubes
		var displace = {val: 0};
		var finalDisplace = {val: vibeDisp}; //change if nonconstant
		this.vibTween = new TWEEN.Tween(displace).to(finalDisplace, timeUnit*(numVibes - i +1));
		
		var xsign = 1;
		var zsign = 1;
		
		//define what it does
		this.vibTween.onUpdate(function(){
			if (displace.val == 0) {
				//do nothing
			} else {
				//move, caling with a scaled disp factor
				this.cube.position.z = this.zorigin + i*i*i/numVibes/numVibes/numVibes*zsign*displace.val;
				this.wireframe.position.z = this.zorigin + i*i*i/numVibes/numVibes/numVibes*zsign*displace.val;
				this.cube.position.x = this.xorigin + i*i*i/numVibes/numVibes/numVibes*xsign*displace.val;
				this.wireframe.position.x =this.xorigin + i*i*i/numVibes/numVibes/numVibes*xsign*displace.val;
			}
		}.bind(this));
		
		//what happens when it's done
		this.vibTween.onComplete(function() {
			//randomize direction of movement
			if (Math.random() > .5) {
				xsign = - xsign;
			}
			if (Math.random() > .5) {
				zsign = - zsign;
			}
			i++;
			
			// set a new duration, such that sum of durations is the time of falling
			var newTime = timeUnit*(numVibes - i + 1);
			this.vibTween.duration = newTime;
			timeSum += newTime;

			if (i == numVibes) { //stop vibrating
				if (!this.defunctSpace){ //ifff it's still on the board, reset it
					this.cube.position.y = 0;
					this.wireframe.position.y = 0;
					this.wireframe.position.z = this.zorigin;
					this.wireframe.position.x = this.xorigin;
					this.cube.position.z = this.zorigin;
					this.cube.position.x = this.xorigin;
				}
			} else {
				//go again, with higher values
				this.vibTween.start();
			}
		}.bind(this));
		
		//go!
		this.vibTween.start();
	}
	
	stopVibrate() { //a hardstop and reset for the shaking for a cube
		//stop
		this.vibTween.stop();
		//reset
		if (!this.defunctSpace) {
			this.cube.position.y = 0;
			this.wireframe.position.y = 0;
		}	
		//recenter
		this.wireframe.position.z = this.zorigin;
		this.wireframe.position.x = this.xorigin;
		this.cube.position.z = this.zorigin;
		this.cube.position.x = this.xorigin;
		
	}
	//start a cube decaying
	decays(player) {
		//if not already decaying, and decayable, set parameters
		if (this.decay ==0 && !this.defunctSpace) {
			this.decay =1;

			this.slammer = player;

			this.color = new THREE.Color(player.color);
			this.cube.material.color.set(this.color);

		}
	}
	
	//decays to get around bug
	decayHelper() {
		decays(player);
	}
	
	//fall from edge; final fall; no slammer. otherwise same as above
	edgeFall() {
		this.defunctSpace = true;
		var finalPos = -50000;
		var position = {y: 0};
		var end = { y: finalPos};
		var tween1 = new TWEEN.Tween(position).to(end, (my.respawnBlock - my.decayed)*my.decayRate);
		if (this.occupied) this.player.stuck();
		tween1.onUpdate(function(){
			this.cube.position.y = position.y;
			this.wireframe.position.y = position.y;
			if (position.y != finalPos) {
				if (this.occupied ) {
					this.player.avatar.position.y = position.y + yfloat;
					this.player.arrow.position.copy(new THREE.Vector3(this.player.avatar.position.x,this.player.avatar.position.y,this.player.avatar.position.z));
				}
			} else {

				if (this.occupied ) {
					this.player.lives--;
					document.getElementById("p1").innerHTML = gameB.players[0].name + ":" + gameB.players[0].lives;
					document.getElementById("p2").innerHTML = gameB.players[1].name + ":" + gameB.players[1].lives;
					if (this.player.lives > 0) {
						this.gameBoard.respawnPlayer(this.player);
					} else {
							
						this.gameBoard.playersChanged = true;
						this.gameBoard.respawnPlayer(this.player);
						var index = this.gameBoard.players.indexOf(this.player);
						if (index > -1) {
						this.gameBoard.players.splice(index, 1);
						scene.remove(this.player.avatar);
							if (this.gameBoard.players.length == 1) {
									sessionStorage.getItem('winnerName');
									sessionStorage.setItem('winnerName', this.gameBoard.players[0].name);
									sessionStorage.getItem('winnerColor');
									sessionStorage.setItem('winnerColor', this.gameBoard.players[0].colorMem);
									sessionStorage.getItem('winnerKills');
									sessionStorage.setItem('winnerKills', this.gameBoard.players[0].kills);
									sessionStorage.getItem('times');
									sessionStorage.setItem('times', time);
									window.location.href = "gameOverScreen.html";
							}
						}
					}
					this.player = null;
					this.occupied = false;
				}
			}
		}.bind(this));
		
		tween1.easing(TWEEN.Easing.Quartic.In);
		
		tween1.start();
		
	}
	//manage decaying edges. The two parallel functions - fall/edgefall, decays/edgeDecays could have been less redundant. There were too many of them. They were too similar, if you know what I mean. I should have been more original with it
	edgeDecays() {
		if (this.decay ==0) {
		this.decay =1;
		
		}
		this.edgeDecaying = true;
		this.defunctSpace = true;
	}
}

//getting around bug
function decayHelper() {
	decays(player);
}

//class of players
class player { 
	constructor(color, name, game, i,n, colorToPass) { 
		
		//a workaround to get the color in the right format
		var d = document.createElement("div");
		d.style.color = color;
		document.body.appendChild(d);
		this.colorMem = colorToPass; //the color in the format the gamOver screen needs
		this.colorName = color; //Color in RGB 
		this.color = window.getComputedStyle(d).color;
		this.color = new THREE.Color(color); //final version in THREE format
		
		this.name = name;
		this.direction = 0; //2 up 1 right 0 down 3 left
		this.lives = my.lives;
		this.x =0;
		this.z=0;
		game.addPlayer(this); //add player to board
		this.noMove = false; //can't move
		this.dying = false; //is it falling?
		this.ysep = (squareH*(my.dimensions+.5)-2*my.playerBuffy)/n; //2d vestige
		this.kills = 0;//[]; //tracks the number of kills a player has gotten. prepped for tracking who as well.

		//sphere part of player
		var geometry = new THREE.SphereGeometry(20, 20, 20);
		var material = new THREE.MeshBasicMaterial({color: this.color});;
		this.avatar = new THREE.Mesh(geometry, material);
		var xpos = this.x*cubeW;
		var zpos = this.z*cubeW;
		this.avatar.position.x = xpos;
		this.avatar.position.z = zpos;
		this.avatar.position.y = yfloat;
		this.avatar.castShadow = true;
		this.avatar.recieveShadow = true; 
		scene.add(this.avatar);

		//cone/arrow part of player
		var arrDir = new THREE.Vector3(0,0,-1);
		var origin = new THREE.Vector3( this.avatar.position.x, this.avatar.position.y, this.avatar.position.z);
		var length = 40;
		var headlength = 35;
		var headwidth = 40;
		this.arrow = new THREE.ArrowHelper(arrDir, origin, length, this.color, headlength, headwidth);
		scene.add(this.arrow);
	}
	moved() {
		//don't let rapidfire movement
		this.noMove = true;
		setTimeout(unfreeze, my.pMoveLag, this);
	}
	stuck() {
		//don't let them move while falling
		this.dying = true;
		setTimeout(unstick, ((my.respawnBlock - my.decayed)*my.decayRate), this);
	}
	
	//move 2d icon when player enters or leaves
	movePlayerIcon(i,n) {
		
		this.ysep = (squareH*(my.dimensions+.5)-2*my.playerBuffy)/n;
		this.icon.x = squareW*(my.dimensions+.5)+.5*my.playerBuffx;
		this.icon.y = this.ysep*(i+.5);
		this.text.y = this.icon.y + playerR*1.2;
		var content = this.name;
		for (var i=0; i<this.lives; i++) {
			content += " *";
		}
		this.text.text = content;
		var widthText = this.text.getMeasuredWidth();
		this.text.x = this.icon.x - widthText/2;
	}
	
	//moves avatar
	movePlayerVisual(i,j) {
		var xval = this.x*cubeW;
		var zval = this.z*cubeW;
		var xnew = (i)*cubeW;
		var znew = (j)*cubeW;
		var xtran = xnew - this.x*cubeW;
		var ztran = znew - this.z*cubeW;
		this.x = i;
		this.z = j;
		var position = {x: xval, z:zval};
		var end = { x:xnew, z: znew};
		
		var tween1 = new TWEEN.Tween(position).to(end, my.pMoveLag);

		tween1.easing(TWEEN.Easing.Cubic.InOut);

		tween1.onUpdate(function(){
			this.avatar.position.x = position.x;
			this.avatar.position.z = position.z;
			this.arrow.position.copy(new THREE.Vector3( this.avatar.position.x, this.avatar.position.y, this.avatar.position.z));
			if (position.x == xnew) {
				this.avatar.position.y = yfloat;
				var origin = new THREE.Vector3( this.avatar.position.x, this.avatar.position.y, this.avatar.position.z);
				this.arrow.position.copy(new THREE.Vector3( this.avatar.position.x, this.avatar.position.y, this.avatar.position.z));
			}
		}.bind(this));
		tween1.start();
	}
	rotate() {
		var conversions = [(new THREE.Vector3(0,0,-1)), (new THREE.Vector3(1,0,0)), (new THREE.Vector3(0,0,1)), (new THREE.Vector3(-1,0,0))];
		this.arrow.setDirection(conversions[this.direction]);
	}
}

//vestigial code
function animateAvatar(x,z,xnew,znew,avatar) {
	//window.alert("animating");
	var position = {x : x, y:yfloat, z:z};
	var end = { x:xnew, y:yfloat, z: znew};
	var tween1 = new TWEEN.Tween(position).to(end, 1000);

	tween1.onUpdate(function(){
		avatar.x = position.x;
		window.alert(position.x);
		avatar.y = position.y;
		//window.alert(position.x);window.alert(position.x);
		avatar.z = position.z; 
	});
	tween1.start();
}
//allow player to move after delay
function unfreeze(p) {
	p.noMove = false;
}
//allow player to move after falling
function unstick(p) {
	p.dying = false;
}

//main class
class game {
	constructor(dimensions) {
		this.board = new Array(dimensions);
		for (var q = 0; q< dimensions; q++) {
			this.board[q] = new Array(10);
		}
		for (var i=0; i<dimensions; i++) {
			for (var j=0; j<dimensions; j++) {
				this.board[i][j] = new gameSpace(this, i, j);
			}
		}
		this.dimensions = dimensions; //how big is the board; redundant
		this.players = []; //list of players
		this.playerIcons = []; //list of player icons - 2d version
		this.playersChanged = true; //2d vestige
		this.numPlayers = 0; //track how many players on board (I'm just lazy)
	}
	//add a player to the board, spawning on a random safe square
	addPlayer(player) {
		if (this.numPlayers < availSpaces) { //only add if there's space
			var a = 0;
			var index1 = 0;
			var index2 = 0;
			do {
				index1 = Math.floor(Math.random()*this.dimensions);
				index2 = Math.floor(Math.random()*this.dimensions);
				a = this.board[index1][index2]
			} while (a.occupied || (a.decay !=0 || a.defunctSpace)); //allowable space. should have been modular
			a.occupied = true;
			a.player = player;
			a.changed = true;
			player.x = index1;
			player.z = index2;
			this.numPlayers++;
			this.players.push(player);
		} else {
			window.alert("that game is full!"); //can't overbook a board
		}
	}
	
	//respawn a player to random safe space; similar to above
	respawnPlayer(player) {
		if (this.numPlayers < availSpaces) {
			var a = 0;
			var index1 = 0;
			var index2 = 0;
			do {
				index1 = Math.floor(Math.random()*this.dimensions);
				index2 = Math.floor(Math.random()*this.dimensions);
				a = this.board[index1][index2]
			} while (a.occupied || (a.decay !=0 || a.defunctSpace));
			a.occupied = true;
			a.player = player;
			a.changed = true;
			player.movePlayerVisual(index1, index2);
		} else {
			window.alert("that game is full!");
		}
	}
	
	//tell a player to move, if chosen direction is safe
	movePlayer(player, direction) {
		//direction is as above - 0 up 1 right 2 down 3 left
		
		//change direction from integer to useful information; should have been an enum
		if (!player.noMove && !player.dying) {
			//set direction
			player.direction = direction;
			var xdelt = 0;
			var ydelt = 0;
			switch(direction) {
				case 0:
					ydelt = -1; //set delta
					player.arrow.setDirection(new THREE.Vector3(0,0,-1)); //change direction of arrow (also could have been a function)
					break;
				case 1:
					xdelt = 1;
					player.arrow.setDirection(new THREE.Vector3(1,0,0));
					break;
				case 2:
					ydelt = 1;
					player.arrow.setDirection(new THREE.Vector3(0,0,1));
					break;
				case 3:
					xdelt = -1;
					player.arrow.setDirection(new THREE.Vector3(-1,0,0));
					break;
			}
			var xnew = player.x + xdelt;
			var ynew = player.z + ydelt;
			
			//on the (remaining) board?
			if ((xnew < fallenRows || ynew < fallenRows) || (xnew >=my.dimensions-fallenRows || ynew >= my.dimensions-fallenRows)) { 
				return false;
			}
			
			//allowed space?
			if (this.board[xnew][ynew].occupied || this.board[xnew][ynew].decay>=my.decayed) {
				return false;
			}
			//change some board variables
			this.board[player.x][player.z].occupied = false;
			this.board[player.x][player.z].player = null;
			this.board[player.x][player.z].changed = true;
		
			//call the big function
			player.movePlayerVisual(xnew, ynew); 
			//notify that has moved, and can't for a bit
			player.moved();
			this.board[player.x][player.z].occupied = true;
			this.board[player.x][player.z].player = player;
			this.board[player.x][player.z].changed = true;
		}
	}
	
	//player action to cause blocks to fall
	slam(player) {
		//people can't slam if falling
		if(!player.dying) {
			//deal with direction
			switch (player.direction) {
				case 2:
					for (var i=player.z+1; i<this.dimensions; i++) {
						this.board[player.x][i].decays(player); //decay all blocks in front of player in chosen direction
					}
					break;
				case 1:
					for (var i=player.x+1; i<this.dimensions; i++) {
					this.board[i][player.z].decays(player);
					}
					break;
				case 0:
					for (var i=player.z-1; i>=0; i--) {
					this.board[player.x][i].decays(player);
					}
					break;
				case 3:
					for (var i=player.x-1; i>=0; i--) {
					this.board[i][player.z].decays(player);
					}
					break; 
			}
		}
	}
	
	//
	edgeDecayController() {
		//select outside rows, then have them fall
		for (var i=fallenRows; i<my.dimensions-fallenRows; i++) {
			for (var j=fallenRows; j<my.dimensions-fallenRows; j++) {
				var innerRow = (i == fallenRows || j == fallenRows);
				var outerRow = (i == (my.dimensions-fallenRows-1) || j == (my.dimensions-fallenRows-1));
				if ( innerRow || outerRow) {
					this.board[i][j].edgeDecays();
					availSpaces--; //take away spaces for space lost
				}
			}
		}
		fallenRows ++;
	}
}

window.onkeyup = function(e){ //set listeners; windowalerts left for context.
	var key = e.keyCode ? e.keyCode : e.which;
	switch (key) {
		case 38:
			//window.alert("up key pressed");
			gameB.movePlayer(gameB.players[0], 0);
			break;
		case 37:
			gameB.movePlayer(gameB.players[0], 3);
			//window.alert("left key pressed");
			break;
		case 40:
			gameB.movePlayer(gameB.players[0], 2);
			//window.alert("down key pressed");
			break;
		case 39:
			gameB.movePlayer(gameB.players[0], 1);
			//window.alert("right key pressed");
			break;
		case 87:
			//window.alert("w key pressed");
			gameB.movePlayer(gameB.players[1], 0);
			break;
		case 65:
			//window.alert("a key pressed");
			gameB.movePlayer(gameB.players[1], 3);
			break;
		case 83:
			//window.alert("s key pressed");
			gameB.movePlayer(gameB.players[1], 2);
			break;
		case 68:
			//window.alert("d key pressed");
			gameB.movePlayer(gameB.players[1], 1);
			break;
		case 32:
			gameB.slam(gameB.players[1]); 
			//window.alert("space key pressed");
			break;
		case 13:
			gameB.slam(gameB.players[0]);
			//window.alert("enter key pressed");
			break;
		case 80:
			window.alert("p key pressed; game paused (press ok to unpause)");
			break;
		case 69:
			//window.alert("e key pressed");
			gameB.players[1].direction = (gameB.players[1].direction+1)%4;
			gameB.players[1].rotate();
			break;
		case 81:
			//window.alert("q key pressed");
			gameB.players[1].direction = (gameB.players[1].direction+3)%4;
			gameB.players[1].rotate();
			//gameB.board[gameB.players[1].x][gameB.players[1].z].changed = true;
			break;
		case 16:
			//window.alert("e key pressed");
			gameB.players[0].direction = (gameB.players[0].direction+1)%4;
			//window.alert(gameB.players[0].x);
			gameB.players[0].rotate();
			break;
		case 17:
			//window.alert("q key pressed");
			gameB.players[0].direction = (gameB.players[0].direction+3)%4;
			gameB.players[0].rotate();
			break;
	}
}

//the function called at an interval to tick up counters on decaying blocks and cause effects
function decayBoard() {
	for (var i=0; i<my.dimensions; i++) {
		for (var j=0; j<my.dimensions; j++) {
			gameB.board[i][j].decayMaint();
		}
	}
}

//defunct; left in for interest. Overlaying screens was a perenial challenge for the project, and layering was never solved.
function setBackground() {
	//from http://stackoverflow.com/questions/19865537/three-js-set-background-image
	// Load the background texture
	var loader = new THREE.TextureLoader();
	var texture = loader.load( 'IMG_5311.JPG' );
	var backgroundMesh = new THREE.Mesh(
		new THREE.PlaneGeometry(2, 2, 0),
		new THREE.MeshBasicMaterial({
			map: texture
		}));

	backgroundMesh .material.depthTest = false;
	backgroundMesh .material.depthWrite = false;

	// Create your background scene
	backgroundScene = new THREE.Scene();
	backgroundCamera = new THREE.Camera();
	backgroundScene.add(backgroundCamera );
	backgroundScene.add(backgroundMesh );
}

//play our sounds
function sound(){
	var sound = document.createElement("AUDIO");
	sound.setAttribute("src","https://drive.google.com/uc?id=0ByBm1rrfnFH5dC1OWVNYek81WVk");
	sound.load();
	sound.play();
}

//initial setup
function init() {
	//check to see if variables are set - if not, redirect to character creation
	var name2 = sessionStorage.getItem('userN1');
	if (name2 == null || name1 == -1) {
		window.location.href = "gameLobby.html";
	} else {
	}
	
	//pull in session information
	var name1 = sessionStorage.getItem('userN2');
	var color2 = sessionStorage.getItem('userC1');
	var color1 = sessionStorage.getItem('userC2');
	
	//Defunct multi-scene code
	HUDw = window.innerWidth;
	HUDh = window.innerHeight;
	HUDz = playerR * 2;
	HUD = new THREE.Scene();
	HUDcamera = new THREE.OrthographicCamera(-HUDw/2, HUDw/2, HUDh/2, -HUDh/2, HUDz, - HUDz);
	
	HUDcamera.position.z = 500;
	HUDcamera.position.y = 350;
	HUDcamera.position.x = 370;
	
	//THIS IS THE MEAT AND POTATOES - THIS SETS UP THE (BASIC) VISualS
	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 10000 );
	camera.position.z = 500;
	camera.position.y = 350;
	camera.position.x = 370;
	renderer = new THREE.WebGLRenderer({ alpha: true });
	renderer.setSize( window.innerWidth*.995, window.innerHeight*.895 );
	renderer.domElement.style.position = 'absolute';
	renderer.domElement.style.zIndex = 1;
	renderer.domElement.style.top = 0;
	renderer.autoclear = false;

	//attempt at setting a light, to better shade the spheres. not sure it did anything.
	var point = new THREE.PointLight(0xffffff, 1, 0);
	var boardCenter = my.dimensions*cubeW/2;
	point.position.set(boardCenter, boardCenter, boardCenter);
	pointcastShadow = true;
	scene.add(point);

	//well, I guess I'll think about HTML again... bleh
	container1 = document.createElement('div');
	document.body.appendChild(container1);
	//add the scene
	container1.appendChild(renderer.domElement);
	
	//vestigial, for 2d
	squareW = (w-my.bufferW)/my.dimensions;
	squareH = (h - my.bufferH)/my.dimensions;

	//set up other parameters
	playerR = Math.floor(Math.min(squareW,squareH)*.35)
	var centerBoard = .5*my.dimensions*cubeW;
	camera.lookAt(new THREE.Vector3(centerBoard, .5*cubeW, centerBoard)); //set camera direction to center of board
	gameB = new game(my.dimensions);
	
	//Create players! Hope to expand to multiplayer and server-client
	var Max = new player(new THREE.Color("#" + color1), name1, gameB,0,1, color1);
	var Drew = new player(new THREE.Color("#" + color2), name2, gameB,1,2, color2);
	
	//set (groan) the html
	document.getElementById("p1").innerHTML = gameB.players[0].name + ":" + gameB.players[0].lives;
	document.getElementById("p2").innerHTML = gameB.players[1].name + ":" + gameB.players[1].lives;
	
	//create ticker, checking parameters for sensibility
	var decayTimer = setInterval(decayBoard, my.decayRate);
	if (my.vibrateTime > my.decayed || (my.respawnBlock - my.decayed) <1) {
		window.alert("invalid block timing parameters");
	}
	//set html
	document.getElementById("time").innerHTML = "0.0s";
	//what does it do
	createjs.Ticker.addEventListener("tick", function() {
		//change the time display
		time ++;
		timetext = "";
		//20ths of a second to H:M:S format
		if (time > 3600*20) {
			timetext += Math.floor(time*5/360000) + ":";
		}
		if (time > 60*20) {
			timetext += Math.floor(time/1200) + ":";
		}
		
		//update html
		timetext += Math.floor((time/20))%60;
		document.getElementById("time").innerHTML =  timetext;
		
		//make edges fall
		if (time > my.edgeDecayFrequency*(fallenRows+1)*20 && ((my.dimensions - 2*(fallenRows+1)) >= my.minimalDimensions )) {
			gameB.edgeDecayController();
		}
	});
}

//animate it
function animate(){
	requestAnimationFrame(animate);
	render();
}

//tell how to render
function render () {
	//update the tweens
	TWEEN.update();
	
	//set screen dimensions (should maybe be somewhere else)
	var SCREEN_W, SCREEN_H;
	SCREEN_W = window.innerWidth;
	SCREEN_H = window.innerHeight;
	
	//set dimensions of screens (was designed for multi-screen situation
	var left,bottom,width,height;
	 
	left = 0;
	bottom = 0;
	width = SCREEN_W;
	height = SCREEN_H;// * .8;
	renderer.setViewport(left,bottom,width,height);
	renderer.setScissor(left,bottom,width,height);
	renderer.setScissorTest (true);
	renderer.setClearColor(0xffffff,0);
	camera.aspect = width/height;
	camera.updateProjectionMatrix();
	renderer.render(scene, camera); 

	left = 0;
	bottom = SCREEN_H * .8;
	width = SCREEN_W;
	height = SCREEN_H * .2;
	renderer.setViewport (left,bottom,width,height); 
	renderer.setScissor(left,bottom,width,height); 
	renderer.setScissorTest (true); 
	camera.aspect = width/height;	
	renderer.setClearColor(0xffffff, 0);
}

animate();

